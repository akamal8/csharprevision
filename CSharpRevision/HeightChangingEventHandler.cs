﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpRevision
{

 public class HeightChangingEventArgs : EventArgs
    {
        public int PreviousHeight { get; set; }
        public int NewHeight { get; set; }

        public object Source { get; set; }

    }

 
}
