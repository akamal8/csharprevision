﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpRevision
{

    public abstract class Shape
    {

        public delegate void HeightChangingEventHandler(object sender, HeightChangingEventArgs e);
        private void OnHeightChanged(int oldHeight, int newHeight)
        {

            if (HeightChanged != null)
            {
                HeightChangingEventArgs arg = new HeightChangingEventArgs();
                arg.PreviousHeight = oldHeight;
                arg.NewHeight = newHeight;
                arg.Source = this;
                HeightChanged(this, arg);
            }

        }
        public event HeightChangingEventHandler HeightChanged;
        public Shape(int _height, int _width)
        {

            Height = _height;
            Width = _width;
        }
        private int height;

        public int Height
        {
            get { return height; }
            set { 
                int preheight = height;
                height = value;
                OnHeightChanged(preheight, height);
            
            }
        }
        
        public int Width { get; set; }

        public virtual void Draw()
        {
            Console.WriteLine("Shape is drawn with Heigh{0} and Width{1}", Height, Width);

        }

    }


    public class Rectangle : Shape
    {

        public Rectangle(int _h, int _w)
            : base(_h, _w)
        {

        }
        public override void Draw()
        {
            Console.WriteLine("A Rectangle is drawn Height {0} and Width {1}",
                base.Height, base.Width);
        }


    }
    class Program
    {

        static void Main(string[] args)
        {
            //Can't create instance from Shape because it is abstract class
            //Shape s = new Shape(5, 10);
            //s.Draw();

            Shape r = new Rectangle((int)HeightLength.MIN, 10);
            r.Draw();

            r.HeightChanged += new Shape.HeightChangingEventHandler(ShowMessage);

            r.Height = (int)HeightLength.MAX;

            Console.ReadLine();


            

        }

        private static void ShowMessage(object sender, HeightChangingEventArgs e)
        {
            Console.WriteLine("Height was changed !");
        }

     }


    enum HeightLength
    {
        MIN = 1 , 
        MAX = 10
    }



}





/*

            Book mybook = new Book(true);
            Console.WriteLine(mybook.title);

            Book hisBook;

            hisBook.author = "Sayed";
            hisBook.title = "Lola ";
            hisBook.price = 323;
            Console.WriteLine(hisBook.title);

            string testString = "Ahmed ka";

            //ChangeString(out testString);
            Console.WriteLine(testString);
          

            foreach (var item in GenerateSomeNumbers())
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();

            int x = 0;

            Console.WriteLine(x);
            ChangeString(x)
                 
        }

        
        private static IEnumerable GenerateSomeNumbers()
        {

            yield return 5;
            for (int i = 0; i < 10; i++)
            {
                yield return i;
            }
        }

        public static  void ChangeString(out string x){

            x = "Changed :S";

        }

        public static void ChangeInt(out string x, int y)
        {

            y = 3;
        }
    }

    struct Book
    {
        public decimal price;
        public string title;
        public string author;

        public Book(bool x) {

            price = 10;
            title = "Ali";
            author = "Kamal";
        }


*/